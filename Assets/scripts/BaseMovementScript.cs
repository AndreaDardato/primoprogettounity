﻿using UnityEngine;

public class BaseMovementScript : MonoBehaviour
{

    public Rigidbody rb;    //rb -> rigidbody


    // Start is called before the first frame update
    //void Start()
    //{

    //    //I want to add some force to my rigidbody
    //    //rb.AddForce(0, 200, 500);   //i parametri sono x, y, z -> quanta forza vogliamo usare

    //}
    //Nel primo tutorial abbiamo usato il valore 2000 per l'asse delle Z, ma adesso lo mettiamo in una variabile
    public float forwardForce = 2000f;  //the value of z
    public float rightValue = 500f;

    // Update is called once per frame
    void FixedUpdate()
    {

        //rb.AddForce(0, 0, forwardForce * Time.deltaTime);    //abbiamo aggiunto Time.deltaTime per far si che non abbiamo
                                                    //differenze di movimenti al secondo dovuti al frame rate

        //if statement that determinate if some keys are pressed
        if(Input.GetKey("d"))
        {
            //move to the right
            rb.AddForce(rightValue * Time.deltaTime, 0, 0);

        }

        if(Input.GetKey("w"))
        {
            rb.AddForce(0, 0, forwardForce * Time.deltaTime);
        }

        if(Input.GetKey("a"))
        {
            rb.AddForce(-rightValue * Time.deltaTime, 0, 0);
        }

        if(Input.GetKey("s"))
        {
            rb.AddForce(0, 0, -forwardForce * Time.deltaTime);
        }



    }
}
