﻿using UnityEngine;

public class followPlayer : MonoBehaviour
{

    //voglio che la cam segui la posizione del nostro character
    //dobbiamo per prima cosa avere un reference al nostro character
    public Transform player;

    //addign a vector3 for the camera -> quanto è lontana la camera dall'oggetto
    //la vogliamo centrata sull'asse delle x, un po' più alta nelle y e un po' indietro (z negativa)
    public Vector3 offset;

    public float sensitivity = 100f;

    public float xRotation = 0f;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Debug.Log(player.position);
        //se scriviamo transform con la prima lettera minuscola ci riferiamo alla posizione dell'oggetto
        //Debug.Log(transform.position);

        //questa è solo la poizione di partenza della cam
        
        transform.position = player.position + offset;

        float mouseX = Input.GetAxis("Mouse X") * sensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * sensitivity * Time.deltaTime; //up and down movement

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        player.Rotate(Vector3.up * mouseX);

    }
}
