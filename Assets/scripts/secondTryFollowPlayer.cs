﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class secondTryFollowPlayer : MonoBehaviour
{

    public Transform player;    //il nostro giocatore

    public Vector3 offset;

    public float sensitivity = 100f;

    public float xRotation = 0f;
    public float yRotation = 0f;

    // Update is called once per frame
    void FixedUpdate()
    {

        transform.position = player.position + offset;

        float mouseX = Input.GetAxis("Mouse X") * sensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * sensitivity * Time.deltaTime; //up and down movement


        xRotation += mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        yRotation += mouseX;
        yRotation = Mathf.Clamp(yRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, yRotation, 0f);
        //player.Rotate(Vector3.up * mouseX);

    }
}
