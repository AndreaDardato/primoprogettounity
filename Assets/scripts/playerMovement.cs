﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
    // Start is called before the first frame update

    public float playerSpeed;   //base speed -> for the first time is the walking speed
    public float walkSpeed = 2f;
    public float sprintValue;
    public float jumpHeight;
    public bool isSprinting;
    public bool isMoving;
    public float mouseSensivity = 3f;
    public float yRot;  //for the rotation of the "head" -> transform with mouseSensivity

    private Rigidbody rigidBody;


    void Start()
    {


        playerSpeed = walkSpeed;

    }

    // Update is called once per frame
    void Update()
    {

        //rotation of the view with mouse
        yRot += Input.GetAxis("Mouse X") * mouseSensivity;  //how many pixel I must rotate my player
        //I passed the new cordinates for updating views
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, yRot, transform.localEulerAngles.z);

        isMoving = false;

        if(Input.GetAxis("Horizontal") > 0.5f || Input.GetAxis("Horizontal") < -0.5f)
        {
            rigidBody.velocity += transform.right * Input.GetAxis("Horizontal") * playerSpeed;
            isMoving = true;
        }
           
    }
}
