﻿using UnityEngine;

public class testRotation : MonoBehaviour
{

    Rigidbody rb;
    public float rotationConst = 50f;

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Rotate(Vector3.up * rotationConst * Time.deltaTime);
    }
}
