﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour
{

    //script that restart the game if we have less than 0 hp

    bool gameHasEnded = false;  //Guardo se la funzione EndGame è già stata richiamata

    public void EndGame()
    {

        if(!gameHasEnded)
        {
            gameHasEnded = !gameHasEnded;
            Debug.Log("HAI PERSO");
            //restart the game
            Restart();
        }

        
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
