﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerCollider : MonoBehaviour
{


    //in questo file avrò anche la gestione del testo

    public float health = 150.0f;
    public float damage = 50.0f;
    public Text loseText;
    public Text hp; //healt text

    void Start()
    {
        loseText.color = Color.yellow;
        hp.color = Color.red;
    }

    void Update()
    {
        hp.text = health.ToString();    //ogni frame controllo la vita del giocatore 
    }

    void OnCollisionEnter(Collision collision)
    {

        //Debug.Log("test per vedere se viene richiamato questo method");

        //ora voglio vedere delle informazioni su quello che ho colpito
        Debug.Log(collision.collider.tag);

        if(collision.collider.tag == "ostacoli")
        {
            //Debug.Log("ho hittato un ostacolo");

            //ogni volta che colpiamo un ostacolo perdiamo 50 di vita
            health -= damage;

            //controllo sulla vita (se è minore 0 uguale a 0 stoppo il gioco)
            if(health <= 0)
            {
                //lose case
                loseText.text = "Hai perso!";

            }


        }

    }


}
