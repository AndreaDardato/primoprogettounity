﻿using UnityEngine;
using UnityEngine.UI;

public class generalPlayerScript : MonoBehaviour
{

    //script di unione tra i vari script scritti precendentemente
    public float health;
    public float damage;
    public Text hp;
    public Text loseText;
    public Rigidbody rb;


    public float forwardForce = 2000.0f;
    public float rightForce = 500.0f;


    // Start is called before the first frame update
    void Start()
    {
        hp.color = Color.red;
        loseText.color = Color.yellow;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //movement script based
        if(Input.GetKey("d"))
        {
            rb.AddForce(rightForce * Time.deltaTime, 0, 0);
        }

        if(Input.GetKey("w"))
        {
            rb.AddForce(0, 0, forwardForce * Time.deltaTime);
        }

        if(Input.GetKey("a"))
        {
            rb.AddForce(-rightForce * Time.deltaTime, 0, 0);
        }

        if(Input.GetKey("s"))
        {
            rb.AddForce(0, 0, -forwardForce * Time.deltaTime);
        }

        if (Input.GetKey("r"))
        {
            FindObjectOfType<GameManagerScript>().EndGame();
        }


        if (rb.position.y < -1)
        {
            //il player è caduto
            FindObjectOfType<GameManagerScript>().EndGame();
        }

        //aggiunta del text hp
        hp.text = health.ToString();

    }



    private void OnCollisionEnter(Collision collision)
    {

        Debug.Log(collision.collider.tag);

        if(collision.collider.tag == "ostacoli")
        {

            health -= damage;

            if(health <= 0)
            {
                loseText.text = "Hai perso! Premi R per \n" +
                    "restartare il livello";
                //test per stoppare il gioco
                hp.text = "0";
                //Time.timeScale = 0; //ferma il gioco -> da mettere nel GameManagerScript


                

            }

        }

    }


}
